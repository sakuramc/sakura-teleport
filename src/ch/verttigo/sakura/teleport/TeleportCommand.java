package ch.verttigo.sakura.teleport;

import cloud.timo.TimoCloud.api.TimoCloudAPI;
import cloud.timo.TimoCloud.api.messages.objects.PluginMessage;
import cloud.timo.TimoCloud.api.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class TeleportCommand implements CommandExecutor {


    @Override
    public boolean onCommand(final CommandSender commandSender, final Command cmd, final String msg, final String[] args) {
        if (commandSender.hasPermission("sakuratp.tp")) {
            if (args.length == 0) {
                commandSender.sendMessage("Please specify the name of the player you want to find");
                return true;
            }
            if (args.length >= 1 && args[0] != null) {
                if (TimoCloudAPI.getUniversalAPI().getPlayer(args[0]) == null) {
                    commandSender.sendMessage(ChatColor.LIGHT_PURPLE + "[SakuraTP]" + ChatColor.RED + " Please enter a valid player!");
                    return true;
                }
                PlayerObject senderPCloud = TimoCloudAPI.getUniversalAPI().getPlayer(commandSender.getName());
                PlayerObject targetPCloud = TimoCloudAPI.getUniversalAPI().getPlayer(args[0]);
                Player target = Bukkit.getPlayer(args[0]);
                Player sender = (Player) commandSender;
                if (senderPCloud.getServer() == targetPCloud.getServer()) {
                    sender.teleport(target);
                    commandSender.sendMessage("§eTeleporting to: §f" + targetPCloud.getName());
                } else {
                    senderPCloud.sendToServer(targetPCloud.getServer());

                    PluginMessage pluginMessage = new PluginMessage("TELEPORT").set("TargetName", targetPCloud.getName()).set("SenderName", senderPCloud.getName());
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Teleport.getInstance(), () -> TimoCloudAPI.getMessageAPI().sendMessageToServer(pluginMessage, targetPCloud.getServer().toString()), 20L);

                    commandSender.sendMessage("§eTeleporting to: §f" + targetPCloud.getName() + " §eon: §f" + targetPCloud.getServer().getName());
                }
            }
        }
        return true;
    }
}
