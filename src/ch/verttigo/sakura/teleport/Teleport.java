package ch.verttigo.sakura.teleport;


import cloud.timo.TimoCloud.api.TimoCloudAPI;
import org.bukkit.plugin.java.JavaPlugin;

public class Teleport extends JavaPlugin {

    public static Teleport instance;

    public static Teleport getInstance() {
        return Teleport.instance;
    }

    public void onEnable() {
        this.getCommand("stp").setExecutor(new TeleportCommand());
        Teleport.instance = this;
        TimoCloudAPI.getMessageAPI().registerMessageListener(new PluginMessageListener(), "TELEPORT");
    }
}
