package ch.verttigo.sakura.teleport;

import cloud.timo.TimoCloud.api.messages.listeners.MessageListener;
import cloud.timo.TimoCloud.api.messages.objects.AddressedPluginMessage;
import cloud.timo.TimoCloud.api.messages.objects.PluginMessage;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PluginMessageListener implements MessageListener {

    @Override
    public void onPluginMessage(AddressedPluginMessage addressedPluginMessage) {
        Bukkit.getScheduler().runTask(Teleport.getInstance(), () -> {
            PluginMessage message = addressedPluginMessage.getMessage();
            Player sender = Bukkit.getPlayer(message.getString("SenderName"));
            Player target = Bukkit.getPlayer(message.getString("TargetName"));
            sender.teleport(target);
        });
    }
}
